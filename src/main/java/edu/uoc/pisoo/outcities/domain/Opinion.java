package edu.uoc.pisoo.outcities.domain;

public class Opinion {
    private TipoOpinion tipo;
    private String comentario;
    private final Plan plan;

    public Opinion(TipoOpinion tipo, String comentario, Plan plan) {
        this.tipo = tipo;
        this.comentario = comentario;
        this.plan = plan;
        plan.addOpinion(this);
    }

    public void modificar(TipoOpinion tipo, String comentario) {
        this.tipo = tipo;
        this.comentario = comentario;
    }

    // Generated getters


    public TipoOpinion getTipo() {
        return tipo;
    }

    public String getComentario() {
        return comentario;
    }

    public Plan getPlan() {
        return plan;
    }

}
