package edu.uoc.pisoo.outcities.domain.test;

import edu.uoc.pisoo.outcities.domain.Zona;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class ZonaTest {

    @Test
    public void testDatosZona(){
        Zona zona = new Zona(19L);
        assertEquals(new Long(19),zona.getId());
    }
}
